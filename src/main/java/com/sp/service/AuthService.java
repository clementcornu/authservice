package com.sp.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sp.model.Auth;

  @Service
  public class AuthService {
      
	  /**
	   * Permet de vérifier que l'utilisateur récupéré est bien le bon, via un appel au service utilisateur
	   *
	   * @param a un objet contenant l'id de l'utilisateur et son mot de passe
	   * @return ret un booleen certifiant la bonne connexion ou non
	   */
      public boolean checkAuth(Auth a) {
    	  RestTemplate restTemplate = new RestTemplate();
    	  boolean ret;
    	  String fooResourceUrl = "http://localhost:8081/users";
    	  ResponseEntity<UserDTO> response = restTemplate.getForEntity(fooResourceUrl + "/" + a.getUser(), UserDTO.class);
    	  if (a.getPassword() == response.getBody().getPassword()) {
    		  ret = true;
    	  }
    	  else { 
    		  ret = false;
    	  }
    	  return ret;
    		 
    	  //assertThat(response.getStatusCode(), equals(HttpStatus.OK));
      }
      
      /**
       * Permet de créer un nouvel objet d'authentification avec le nom d'utilisateur et son mdp
       * 
       * @param idconnexion le nom/pseudo de l'utilisateur
       * @param password le mot de passe de l'utilisateur
       * @return l'objet d'identification créé
       */
      public Auth getAuth(String idconnexion,String password) {
          Auth a =new Auth(idconnexion,password);
          return a;
      }
      
      /**
       * 
       * Classe permettant de créer un DTO (Data Transfer Object) relatif à un utilisateur
       * Cette classe est normalement dans une librairie partagée entre tous les microservices, mais
       * dans le cas du projet, chaque microservice nécessitant un user DTO aura créé le sien
       * Pour le service Authentification, on n'a besoin que du username et du password de 
       * l'utilisateur.
       *
       */
      public static class UserDTO {
    	  private String username;
    	  private String password;
    	  public UserDTO() {}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
    	  
    	  
      }

  }

