package com.sp.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.AccessToken;
import com.sp.model.Auth;
import com.sp.service.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthRestCtr {
	
	@Autowired
	private AuthService aService;
	
	@GetMapping
	public AccessToken getAuth(@RequestParam String user,@RequestParam String password) {
		Auth a = new Auth();
		AccessToken access = new AccessToken();
		a = aService.getAuth(user, password);
		System.out.println(a.toString());
		if (aService.checkAuth(a)){
			access.setToken(access.generateToken()); 
		}
		return access;
	}

}

